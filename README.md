# Jinkilante

## What is it ?

Jinkilante can watch your system and alert you when a process is consuming
too many resources.

## Requirements

- Rust

## Installation

Run the following command: `cargo build --release`

Add the target directory to your path: `export PATH=$PATH:./target/release`

For a permanent export, add the previous command to your configuration files,
such as `~/.bashrc` or `~/.zshrc`.

## How to use ?

Check the following command: `jinkilante -h`
